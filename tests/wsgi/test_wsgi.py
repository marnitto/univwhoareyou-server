# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import json
import StringIO

import flask
import pytest
import requests
import responses

from wsgi import model
from wsgi import wsgi
from wsgi import api
from wsgi.wsgi import app
from tests.mock import MockDatabase

google_gcm_server = 'https://gcm-http.googleapis.com/gcm/send'


@pytest.fixture
def client():
    app.testing = True
    return app.test_client()


@pytest.fixture
def mockdb(monkeypatch):
    m = MockDatabase(Base=model.Base)
    monkeypatch.setattr(wsgi, 'session', m.session)
    monkeypatch.setattr(api, 'session', m.session)
    return m


@responses.activate
def test_gcm_call_from_webapp(client):
    responses.add(responses.POST, google_gcm_server,
        status=200, body='{}', content_type='application/json; charset=UTF-8')

    client.post('/gcm', data=dict(
        request_id='reqid',
        title='t',
        message='m'
    ))

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == google_gcm_server


@responses.activate
def test_gcm_post_data(client):
    responses.add(responses.POST, google_gcm_server,
        status=200,
        body=json.dumps({'success': 1}),
        content_type='application/json; charset=UTF-8'
    )

    form = {'request_id': 'R', 'title': 'T', 'message': 'M'}
    with client:
        client.post('/gcm', data=form)

        assert len(responses.calls) == 1
        req = responses.calls[0].request
        assert req.url == google_gcm_server

        body = json.loads(req.body)
        assert form['request_id'] == body['to']
        assert form['title'] == body['data']['title']
        assert form['message'] == body['data']['message']


@pytest.mark.parametrize('uri', [
    '/',
])
def test_login_required_redirects_login(client, uri):
    with client:
        rv = client.get(uri)
        assert rv.status_code == 302


def test_login(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        rv = client.post('/login', data=data, follow_redirects=True)
        assert '{}님 환영합니다'.format(data['id']) in rv.data.decode('utf-8')


# FIXME: check if login has been failed
def test_login_unknown_id(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}

    with client:
        rv = client.post('/login', data=data, follow_redirects=True)
        assert '{}님 환영합니다'.format(data['id']) not in \
            rv.data.decode('utf-8')


def test_login_invalid_password(client, mockdb):
    data = {'id': 'user', 'pw': 'invalid_pass'}
    u = model.User(name=data['id'])
    u.set_password('VALID_PASSWORD')
    mockdb.add_object(u)

    with client:
        rv = client.post('/login', data=data, follow_redirects=True)
        assert '{}님 환영합니다'.format(data['id']) not in \
            rv.data.decode('utf-8')


def test_logout(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        client.post('/login', data=data, follow_redirects=True)
        rv = client.get('/logout', follow_redirects=True)
        assert '{}님 환영합니다'.format(data['id']) not in \
            rv.data.decode('utf-8')


def test_register_renders_main_if_no_form(client):
    with client:
        rv = client.get('/register', follow_redirects=True)
        assert 'Register' in rv.data.decode('utf-8')


def test_register_filter_duplicate_id(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        rv = client.post('/register', data=data, follow_redirects=True)
        assert 'Register' in rv.data.decode('utf-8')
        assert 'Already registered' in rv.data.decode('utf-8')


def test_register_adds_to_database(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}

    with client:
        rv = client.post('/register', data=data, follow_redirects=True)
        assert '{}님 환영합니다'.format(data['id']) in rv.data.decode('utf-8')

        client.get('/logout')
        rv = client.post('/login', data=data, follow_redirects=True)
        assert '{}님 환영합니다'.format(data['id']) in rv.data.decode('utf-8')


@pytest.mark.parametrize('monitoring,expect', [
    (True, '안전하게 감시하고'),
    (False, '감시하지 않고')
])
def test_index_show_monitoring_status(client, mockdb, monitoring, expect):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'], monitoring=monitoring)
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        rv = client.post('/login', data=data, follow_redirects=True)
        assert expect in rv.data.decode('utf-8')


# TODO: ^C^V test code, need refactoring?
@pytest.mark.parametrize('intruding,expect', [
    (False, '아니오'),
    (True, '예')
])
def test_index_show_intruding_status(client, mockdb, intruding, expect):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'], intruding=intruding)
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        rv = client.post('/login', data=data, follow_redirects=True)
        assert expect in rv.data.decode('utf-8')


def test_index_show_intruding_gallery_link(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'], intruding=True)
    u.set_password(data['pw'])
    u = mockdb.add_object(u)
    inc = mockdb.add_object(model.Incident(user_id=u.id))

    with client:
        rv = client.post('/login', data=data, follow_redirects=True)
        assert '/gallery/%s' % inc.id in rv.data.decode('utf-8')


def test_gallery_latest_redirect_to_latest_incident_id(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    u = mockdb.add_object(u)
    inc = mockdb.add_object(model.Incident(user_id=u.id))

    expect_redirect = '/gallery/%s' % inc.id

    with client:
        client.post('/login', data=data)
        rv = client.get('/gallery')
        assert '#'+str(inc.id) in rv.data.decode('utf-8')


def test_gallery_post_login(client, mockdb):
    data = {'id': 'user', 'pw': 'pass'}
    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    u = mockdb.add_object(u)
    inc = mockdb.add_object(model.Incident(user_id=u.id))

    img = model.Image(incident_id=inc.id)
    img.set_hash(StringIO.StringIO('asdf'))
    img = mockdb.add_object(img)

    with client:
        rv = client.post('/gallery', data=data)
        assert img.filename in rv.data.decode('utf-8')
