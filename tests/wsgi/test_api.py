# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import copy
import datetime
import hashlib
import json
import os
import shutil
import StringIO
import tempfile

import pytest
import responses

from tests.wsgi.test_wsgi import client
from wsgi import model
from wsgi import api
from wsgi import gcm
from tests.mock import MockDatabase


@pytest.fixture
def chroot(request, monkeypatch):
    '''Change root directory to tmpfs.'''

    cwd = os.getcwd()
    tmproot = tempfile.mkdtemp()
    os.makedirs(os.path.join(tmproot, 'wsgi', 'static'))
    monkeypatch.setattr(api.os, 'getcwd', lambda: tmproot)

    def fin():
        shutil.rmtree(tmproot)

    request.addfinalizer(fin)
    return os.path.join(tmproot, 'wsgi', 'static')


@pytest.fixture
def mockdb(monkeypatch):
    m = MockDatabase(Base=model.Base)
    monkeypatch.setattr(api, 'session', m.session)
    return m


@pytest.mark.parametrize('method,uri', [
    ('post', '/api/base/log'),
    ('post', '/api/base/image'),
    ('get', '/api/base/image/1')
])
def test_api_base_requires_login(client, method, uri):
    with client:
        rv = getattr(client, method)(uri)
        assert rv.status_code == 403


@pytest.mark.parametrize('missing', ['id', 'pw'])
def test_api_baselogin_requires_id_and_pw(client, mockdb, missing):
    data = dict(id='id', pw='pw')
    data.pop(missing)

    with client:
        rv = client.post('/api/base/login', data=data)
        assert rv.status_code == 400


def test_api_baselogin_returns_false_if_no_id(client, mockdb):
    data = dict(id='id', pw='pw')
    with client:
        rv = client.post('/api/base/login', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == False


def test_api_baselogin_returns_false_if_password_mismatch(client, mockdb):
    data = dict(id='id', pw='pw')

    u = model.User(name=data['id'])
    u.set_password('OTHER_PASSWORD')
    mockdb.add_object(u)

    with client:
        rv = client.post('/api/base/login', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == False


def test_api_baselogin_returns_true_if_logged_in(client, mockdb):
    data = dict(id='id', pw='pw')

    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        rv = client.post('/api/base/login', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == True


def test_api_baselog_creates_incident_if_not_exists(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    u = mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/log', data=dict(message='message'))

    sess = mockdb.session()
    inc = sess.query(model.Incident).one()
    assert inc.user_id == u.id


def test_api_baselog_reuses_recent_incident(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    u.incidences.append(model.Incident())
    u = mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/log', data=dict(message='message'))

    sess = mockdb.session()
    assert sess.query(model.Incident).count() == 1


def test_api_baselog_dont_reuse_older_than_30min_incident(client, mockdb):
    delta = datetime.timedelta(minutes=60)

    u = model.User(name='id')
    u.set_password('pw')
    u.incidences.append(model.Incident(
        end_time=datetime.datetime.utcnow()-delta))
    u = mockdb.add_object(u)
    print u.incidences[0].end_time

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/log', data=dict(message='message'))

    sess = mockdb.session()
    inc2 = sess.query(model.Incident).all()[0]
    print datetime.datetime.utcnow()
    print inc2.id, inc2.user_id, inc2.end_time, inc2.start_time
    assert sess.query(model.Incident).count() == 2


def test_api_baselog_stores_message(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    u = mockdb.add_object(u)

    expect_message = 'Message from terminal'

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/log', data=dict(message=expect_message))

    sess = mockdb.session()
    u2 = sess.query(model.User).one()
    assert len(u2.incidences) == 1
    inc = u2.incidences[0]
    assert len(inc.logs) == 1
    assert inc.logs[0].message == expect_message


def test_api_baselog_incidence_count(client, mockdb):
    sess = mockdb.session()
    delta = datetime.timedelta(minutes=60)

    users_data = (('id1', 'pw1'), ('id2', 'pw2'))
    users = []
    for udata in users_data:
        username, userpw = udata
        u = model.User(name=username)
        u.set_password(userpw)
        mockdb.add_object(u)

        with client:
            client.post('/api/base/login', data=dict(id=username, pw=userpw))
            client.post('/api/base/log', data=dict(message='asdf'))

        uq = sess.query(model.User).filter_by(name=username).one()
        assert len(uq.incidences) == 1
        assert uq.incidences[0].count == 1


def test_api_baseimage_requires_image(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        rv = client.post('/api/mobile/auth')
        assert rv.status_code == 400  # Bad request


def test_api_baseimage_stores_image_to_server(client, chroot, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    mockdb.add_object(u)

    test_image_content = b'TEST_IMAGE_CONTENT'
    data = dict(image=((StringIO.StringIO(test_image_content), 'test.jpg')))

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        rv = client.post('/api/base/image', data=data)
        res = json.loads(rv.data.decode('utf-8'))

    assert 'result' in res
    assert res['result'] == 'success'

    sess = mockdb.session()
    img = sess.query(model.Image).one()
    expect_path = os.path.join(chroot, res['path'][8:])  # ignore /static/

    assert expect_path.endswith('.jpg')
    assert os.path.exists(expect_path)
    assert test_image_content == open(expect_path, 'rb').read()


def test_api_baseimage_ignore_duplicated_image(client, chroot, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    mockdb.add_object(u)

    test_image_content = b'TEST_IMAGE_CONTENT'
    data = dict(image=((StringIO.StringIO(test_image_content), 'test.jpg')))
    data_ = copy.deepcopy(data)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/image', data=data)
        rv = client.post('/api/base/image', data=data_)  # StringIO re-open
        res = json.loads(rv.data.decode('utf-8'))

    sess = mockdb.session()
    assert sess.query(model.Image).count() == 1


def test_api_baseimagelist_lists_images(client, chroot, mockdb):
    expect_images = [(b'IMAGE1', 'file1.jpg'), (b'IMAGE2', 'file2.jpg')]

    u = model.User(name='id')
    u.set_password('pw')
    mockdb.add_object(u)

    with client:
        # Upload image
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/image', data=dict(image=(
            (StringIO.StringIO(expect_images[0][0]), expect_images[0][1]))))
        client.post('/api/base/image', data=dict(image=(
            (StringIO.StringIO(expect_images[1][0]), expect_images[1][1]))))

        # Query
        rv = client.get('/api/base/image/1')
        res = json.loads(rv.data.decode('utf-8'))
        assert len(res['result']) == len(expect_images)

    for imguri, eimg in zip(res['result'], expect_images):
        data = open(os.path.join(chroot, imguri[8:]), 'rb').read()
        assert data == eimg[0]


def test_api_baseimagelist_lists_images_group_by_count(
    client, chroot, mockdb):
    images = [(b'IMAGE1', 'file1.jpg'), (b'IMAGE2', 'file2.jpg')]
    expect_image = images[1]

    u = model.User(name='id')
    u.set_password('pw')
    u = mockdb.add_object(u)

    with client:
        # Upload first image
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.post('/api/base/image', data=dict(image=(
            (StringIO.StringIO(images[0][0]), images[0][1]))))

        # Change first incident's end_time for new Incident instance
        sess = mockdb.session()
        inc = sess.query(model.Incident).one()
        inc.end_time = datetime.datetime.utcnow() - \
            datetime.timedelta(minutes=60)
        sess.add(inc)

        # Upload second image
        client.post('/api/base/image', data=dict(image=(
            (StringIO.StringIO(images[1][0]), images[1][1]))))

        # Check incident1 --> [file1.jpg], incident2 --> [file2.jpg]
        for idx, img in enumerate(images):
            rv = client.get('/api/base/image/%d' % (idx+1))
            res = json.loads(rv.data.decode('utf-8'))
            assert len(res['result']) == 1

            data = open(os.path.join(chroot, res['result'][0][8:]),
                'rb').read()
            assert data == img[0]


def test_api_baseimagelist_separates_image_based_on_userid(
    client, chroot, mockdb):
    images = [(b'IMAGE1', 'file1.jpg'), (b'IMAGE2', 'file2.jpg')]

    u1 = model.User(name='id1')
    u1.set_password('pw1')
    mockdb.add_object(u1)
    u2 = model.User(name='id2')
    u2.set_password('pw2')
    mockdb.add_object(u2)

    # User1 --> Image1
    with client:
        client.post('/api/base/login', data=dict(id='id1', pw='pw1'))
        client.post('/api/base/image', data=dict(image=(
            (StringIO.StringIO(images[0][0]), images[0][1]))))

    # User2 --> Image2
    with client:
        client.post('/api/base/login', data=dict(id='id2', pw='pw2'))
        client.post('/api/base/image', data=dict(image=(
            (StringIO.StringIO(images[1][0]), images[1][1]))))

        # Check User2's incident contains image2 only
        rv = client.get('/api/base/image/1')
        res = json.loads(rv.data.decode('utf-8'))
        assert len(res['result']) == 1

        data = open(os.path.join(chroot, res['result'][0][8:]),
            'rb').read()
        assert data == b'IMAGE2'


@pytest.mark.parametrize('intruding,monitoring', [
    (False, False),
    (False, True),
    (True, False),
    (True, True),
])
def test_api_basestatus_get(client, mockdb, intruding, monitoring):
    u = model.User(name='id')
    u.set_password('pw')
    u.intruding = intruding
    u.monitoring = monitoring
    mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        rv = client.get('/api/base/status')
        res = json.loads(rv.data.decode('utf-8'))

        assert res['result'] == 'success'
        assert res['intruding'] == intruding
        assert res['monitoring'] == monitoring


@pytest.mark.parametrize('cmd,arg,expect', [
    ('intruding', 'on', True),
    ('intruding', 'off', False),
    ('monitoring', 'on', True),
    ('monitoring', 'off', False)
])
def test_api_basestatus_set(client, mockdb, cmd, arg, expect):
    u = model.User(name='id')
    u.set_password('pw')
    setattr(u, cmd, not expect)
    mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        # Set status
        client.get('/api/base/status/%s/%s' % (cmd, arg))
        rv = client.get('/api/base/status')
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == 'success'
        assert res[cmd] == expect


def test_api_basestatus_set_monitor_off_makes_intruding_off(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    u.intruding = u.monitoring = True
    mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        client.get('/api/base/status/monitoring/off')  # Monitoring off

        rv = client.get('/api/base/status')
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == 'success'
        assert res['intruding'] == False
        assert res['monitoring'] == False


@pytest.mark.parametrize('invalid_cmd', [
    'asdf',
    'intrud1ng',
    'mon1toring',
    'monitor',
    'intrude',
    'id',
    'name'
])
def test_api_basestatus_set_deny_invalid_command(client, mockdb, invalid_cmd):
    u = model.User(name='id')
    u.set_password('pw')
    mockdb.add_object(u)

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))

        rv = client.get('/api/base/status/%s/on' % invalid_cmd)
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == 'error'


@responses.activate
def test_api_basestatus_intruding_on_trigger_gcm(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    u.devices = [model.UserDevice(user_id=u.id, notify_key='NOTIFY_KEY')]
    mockdb.add_object(u)

    sess = mockdb.session()
    u = sess.query(model.User).one()
    ud = sess.query(model.UserDevice).one()

    responses.add(
        responses.POST, gcm.google_gcm_server,
        status=200, body='{}', content_type='application/json; charset=UTF-8')

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        rv = client.get('/api/base/status/intruding/on')
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == 'success'
        assert res['notify_count'] == 1

        assert len(responses.calls) == 1
        assert responses.calls[0].request.url == gcm.google_gcm_server


@responses.activate
def test_api_basestatus_intruding_on_trigger_all_devices(client, mockdb):
    u = model.User(name='id')
    u.set_password('pw')
    # Register 5 devices
    u.devices = [
        model.UserDevice(user_id=u.id, notify_key='NOTIFY_KEY'+str(i)) \
        for i in range(5)]
    mockdb.add_object(u)

    responses.add(
        responses.POST, gcm.google_gcm_server,
        status=200, body='{}', content_type='application/json; charset=UTF-8')

    with client:
        client.post('/api/base/login', data=dict(id='id', pw='pw'))
        rv = client.get('/api/base/status/intruding/on')
        res = json.loads(rv.data.decode('utf-8'))
        assert res['result'] == 'success'
        assert res['notify_count'] == 5

        assert len(responses.calls) == 5


def test_api_mobileauth_requires_requestid(client):
    with client:
        rv = client.post('/api/mobile/auth', data={})
        assert rv.status_code == 400  # Bad request


def test_api_mobileauth_returns_empty_string_if_not_registered(client, mockdb):
    data = {'request_id': 'asdf'}

    with client:
        rv = client.post('/api/mobile/auth', data=data)
        res = json.loads(rv.data.decode('utf-8'))

        assert 'result' in res
        assert res['result'] == ''


def test_api_mobileauth_returns_userid_if_found(client, mockdb):
    expect_username = 'USERNAME'
    notify_key = 'NOTIFY_KEY'

    # Register UserID
    u = model.User(name=expect_username)
    u.set_password('PASSWORD')
    u = mockdb.add_object(u)
    ud = model.UserDevice(user_id=u.id, notify_key=notify_key)
    mockdb.add_object(ud)

    data = {'request_id': notify_key}

    with client:
        rv = client.post('/api/mobile/auth', data=data)
        res = json.loads(rv.data.decode('utf-8'))

        assert 'result' in res
        assert res['result'] == expect_username


@pytest.mark.parametrize('missing', ['request_id', 'id', 'pw'])
def test_api_mobileregister_requires_argument(client, missing):
    data = dict(request_id='request_id', id='id', pw='pw')
    data.pop(missing)

    with client:
        rv = client.post('/api/mobile/register', data=data)
        assert rv.status_code == 400  # Bad request


def test_api_mobileregister_register_new_account(client, mockdb):
    data = dict(request_id='request_id', id='id', pw='pw')

    with client:
        rv = client.post('/api/mobile/register', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res[u'result'] == u'success'

    sess = mockdb.session()
    u = sess.query(model.User).one()
    ud = sess.query(model.UserDevice).one()
    assert u.name == data['id']
    assert u.check_password(data['pw'])
    assert ud.notify_key == data['request_id']
    assert ud.user_id == u.id


def test_api_mobileregister_register_existing_account(client, mockdb):
    data = dict(request_id='request_id', id='id', pw='pw')

    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    mockdb.add_object(u)

    with client:
        rv = client.post('/api/mobile/register', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res[u'result'] == u'success'

    sess = mockdb.session()
    u2 = sess.query(model.User).one()
    ud = sess.query(model.UserDevice).one()
    assert u2.id == u.id
    assert ud.user_id == u2.id
    assert ud.notify_key == data['request_id']


def test_api_mobileregister_deny_invalid_passowrd(client, mockdb):
    data = dict(request_id='request_id', id='id', pw='pw')

    u = model.User(name=data['id'])
    u.set_password('not_matching_password')
    mockdb.add_object(u)

    with client:
        rv = client.post('/api/mobile/register', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res[u'result'] == u'fail'


def test_api_mobileregister_does_not_add_duplicated_notifykey(client, mockdb):
    data = dict(request_id='request_id', id='id', pw='pw')

    u = model.User(name=data['id'])
    u.set_password(data['pw'])
    u = mockdb.add_object(u)
    ud = model.UserDevice(user_id=u.id, notify_key=data['request_id'])
    ud = mockdb.add_object(ud)

    with client:
        rv = client.post('/api/mobile/register', data=data)
        res = json.loads(rv.data.decode('utf-8'))
        assert res[u'result'] == u'ignore'

    # Assert no entry has been added
    sess = mockdb.session()
    assert sess.query(model.User).count() == 1
    assert sess.query(model.UserDevice).count() == 1
