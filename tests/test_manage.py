# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os
import subprocess
import time

import magic
import pytest
import sqlalchemy

from wsgi import model


in_docker = os.path.exists('/.dockerinit')


@pytest.mark.skipif(not in_docker,
    reason='Test should be run in docker container')
def test_dbimage_generate_schema_png():
    subprocess.check_call(['python', 'manage.py', 'dbimage'])

    expect_png = '/srv/wsgi/static/schema.png'
    assert not os.path.exists('/tmp/schema.dot')
    assert os.path.exists(expect_png)
    assert magic.from_file(expect_png, mime=True) == 'image/png'


@pytest.mark.skipif(not in_docker,
    reason='Test should be run in docker container')
def test_install_generates_schema():
    engine = sqlalchemy.create_engine(
        'mysql://%s:%s@mariadb/%s' % (
        os.getenv('MARIADB_1_ENV_MYSQL_USER'),
        os.getenv('MARIADB_1_ENV_MYSQL_PASSWORD'),
        os.getenv('MARIADB_1_ENV_MYSQL_DATABASE')
    ))

    metadata = sqlalchemy.MetaData()
    for retry in range(5):
        try:
            metadata.reflect(engine)
        except:
            time.sleep(5)

    # Connection OK
    assert retry != 5

    # No tables
    assert len(metadata.tables) == 0

    subprocess.check_call(['python', 'manage.py', 'install'])

    # Table should be built
    metadata = sqlalchemy.MetaData()
    metadata.reflect(engine)
    assert len(metadata.tables) == len(model.Base.metadata.tables)
