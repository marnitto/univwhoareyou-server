# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os

import pytest

from wsgi import tasks


in_docker = os.path.exists('/.dockerinit')
