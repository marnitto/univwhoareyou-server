#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import json
import os
import functools
import logging

import flask
from flask import Flask
import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker

import gcm
import model
import api
from model import session


logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.register_blueprint(api.api, url_prefix='/api')
app.secret_key = 'fkwmqpfl_vkdlsms_Tmfprldlqlsek'


@app.teardown_appcontext
def cleanup_sqlalchemy_session(exception=None):
    model.session.remove()


def login_required(f):
    @functools.wraps(f)
    def login_check(*arg, **kwarg):
        if 'user' in flask.session:
            return f(*arg, **kwarg)

        # No session, try to POST login
        if flask.request.method == 'POST' and \
            'id' in flask.request.form and \
            'pw' in flask.request.form and \
            json.loads(api.api_base_login().data.decode('utf-8'))['result']:
            flask.session['user'] = flask.request.form['id']
            return f(*arg, **kwarg)

        flask.flash(u'Login please!')
        return flask.redirect(flask.url_for('.page_login'))

    return login_check


@app.route('/')
@login_required
def page_index():
    sess = session()
    try:
        u = sess.query(model.User).filter_by(name=flask.session['user']).one()
    except NoResultFound:
        flask.session.pop('user', None)
        return flask.redirect(flask.url_for('.page_login'))

    ret = {'title': 'Dashboard', 'user': u}
    if u.intruding:
        inc = api.recent_incident(u)
        ret['incident'] = inc.id

    return flask.render_template('index.html', **ret)


@app.route('/login', methods=['GET', 'POST'])
def page_login():
    sess = session()

    def login_check():
        try:
            u = sess.query(model.User) \
                .filter_by(name=form['id']).one()
            return u.check_password(form['pw'])
        except NoResultFound:
            return False

    if flask.request.method == 'POST':
        # Login processing
        form = flask.request.form
        if login_check():
            flask.session['user'] = form['id']
            return flask.redirect(flask.url_for('.page_index'))

    return flask.render_template('login.html', title=u'Login')


@app.route('/logout')
def page_logout():
    flask.session.pop('user', None)
    return flask.redirect(flask.url_for('.page_index'))


@app.route('/register', methods=['GET', 'POST'])
def page_register():
    sess = session()

    if flask.request.method == 'GET':
        return flask.render_template('register.html', title=u'Register')

    # POST - registration form filled
    form = flask.request.form
    try:
        u = sess.query(model.User) \
            .filter_by(name=form['id']).one()

        # No exception; User already exists
        flask.flash(u'Already registered.')
        return flask.redirect(flask.url_for('.page_register'))
    except NoResultFound:
        pass

    u = model.User(name=form['id'])
    u.set_password(form['pw'])
    with sess.begin():
        sess.add(u)

    flask.flash(u'Registered')
    flask.session['user'] = form['id']
    return flask.redirect(flask.url_for('.page_index'))


@app.route('/gcm', methods=['GET', 'POST'])
def page_gcm():
    result = ''
    if flask.request.method == 'POST':
        form = flask.request.form
        result = gcm.send(form['request_id'], form['title'], form['message'])

    return flask.render_template('gcm.html', title=u'Sample GCM pusher',
        gcm_result=result)


@app.route('/gallery/<int:no>', methods=['GET', 'POST'])
@login_required
def page_gallery(no):
    sess = session()
    images = sess.query(model.Image).filter_by(incident_id=no)

    return flask.render_template('gallery.html',
        title='Gallery', no=no, images=images)


@app.route('/gallery', methods=['GET', 'POST'])
@login_required
def page_gallery_latest():
    sess = session()
    try:
        user = sess.query(model.User.id) \
            .filter_by(name=flask.session['user']) \
            .one()
    except NoResultFound:
        flask.session.pop['user']
        return flask.redirect(flask.url_for('.page_index'))

    inc = sess.query(model.Incident).filter_by(user_id=user.id) \
        .order_by(sqlalchemy.desc(model.Incident.id)) \
        .first()

    if not inc:
        return flask.redirect(flask.url_for('.page_index'))

    return page_gallery(inc.id)
