# -*- coding: utf-8 -*-

from __future__ import absolute_import

import celery


app = celery.Celery('tasks')
app.config_from_object('wsgi.celeryconfig')


@app.task
def add(a, b):
    return a + b
