# -*- coding: utf-8 -*-
# Developer shell script

import os
import sys


def build(component=''):
    os.system('docker-compose build')
    os.system('docker-compose stop %s' % component)
    os.system('docker-compose rm -f %s' % component)
    os.system('docker-compose up -d')


def test():
    os.system(
        'docker exec -i -t univwhoareyouserver_web_1 python manage.py test')


def localtest():
    os.system('python setup.py test --pytest-args="tests/wsgi/"')


def shell():
    os.system('docker exec -i -t --user root univwhoareyouserver_web_1'
        ' /bin/bash')



if __name__ == '__main__':
    if sys.argv[1] == 'rebuild':
        build(''.join(sys.argv[2:]))
    elif sys.argv[1] == 'test':
        test()
    elif sys.argv[1] == 'retest':
        build()
        test()
    elif sys.argv[1] == 'shell':
        shell()
    elif sys.argv[1] == 'localtest':
        localtest()

    else:
        print('Usage: %s rebuild|test|retest' % sys.argv[1])
